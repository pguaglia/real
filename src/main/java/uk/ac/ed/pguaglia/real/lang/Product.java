package uk.ac.ed.pguaglia.real.lang;

import java.util.Collections;
import java.util.Set;

import uk.ac.ed.pguaglia.real.Utils;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;

public class Product extends BinaryOperation {

	public Product ( Expression left, Expression right ) {
		super( left, right, Expression.Type.PRODUCT );
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> leftAttrs = leftOperand.signature(schema);
		Set<String> rightAttrs = rightOperand.signature(schema);
		if (Collections.disjoint(leftAttrs, rightAttrs)) {
			Set<String> attrs = Utils.clone(leftAttrs);
			attrs.addAll(rightAttrs);
			return attrs;
		} else {
			throw SchemaException.noDisjointAttributes(this, schema);
		}
	}
}
