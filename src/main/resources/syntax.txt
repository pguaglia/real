┌──────────────────────────────────────────────────────────────────────────────┐
│ RELATIONAL ALGEBRA GRAMMAR                                                   │
└──────────────────────────────────────────────────────────────────────────────┘

  TABLE_NAME  →  case-sensitive alphanumeric string that:
                   ‣ starts with a letter
                   ‣ may contain the underscore character "_"
                   ‣ does not contain spaces of any kind
              
  EXPRESSION  →  TABLE_NAME                         # base table (or view)
              →  <S>[ CONDITION ]( EXPRESSION )     # selection
              →  <P>[ ATTR_LIST ]( EXPRESSION )     # projection
              →  <R>[ REPL_LIST ]( EXPRESSION )     # renaming
              →  <E>( EXPRESSION )                  # duplicate elimination
              →  EXPRESSION BINARY_OP EXPRESSION    # binary operation
              →  ( EXPRESSION )                     # parenthesized expression
              
  BINARY_OP   →  <X>    # cross product
              →  <U>    # union
              →  <D>    # difference
              →  <I>    # intersection
              
  ATTR_NAME   →  same rules as for table names
              
  STRING      →  anything enclosed in single quotes

  INTEGER     →  non-empty sequence of digits

  CONSTANT    →  STRING
              →  INTEGER

  TERM        →  ATTR_NAME
              →  CONSTANT

  ATTR_LIST   →  comma-separated list of ATTR_NAMEs

  REPLACEMENT →  ATTR_NAME -> ATTR_NAME

  REPL_LIST   →  comma-separated list of REPLACEMENTs

  CONDITION   →  TERM = TERM              # equality comparison
              →  TERM < TERM              # less-than comparison
              →  CONDITION & CONDITION    # conjunction
              →  CONDITION | CONDITION    # disjunction
              →  ~ CONDITION              # negation
              →  ( CONDITION )            # parenthesized condition

┌──────────────────────────────────────────────────────────────────────────────┐
│ UNARY OPERATIONS: USAGE OF PROJECTION, SELECTION, RENAMING                   │
└──────────────────────────────────────────────────────────────────────────────┘

  <S>    selection

         Usage: <S>[selection-condition](EXPRESSION)

         Example: <S>[A=B](R)
           ‣ selects the rows of R where A=B

  <P>    projection

         Usage: <P>[comma-separated-list-of-attributes](EXPRESSION)

         Example: <P>[A,B](R)
           ‣ projects columns A and B from R

  <R>    renaming

         Usage: <R>[comma-separated-list-of-replacements](EXPRESSION)

         Example: <R>[A->B,B->C](R<U>S)
           ‣ renames attributes A and B of R<U>S to B and C, respectively
