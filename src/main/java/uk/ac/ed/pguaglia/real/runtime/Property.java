package uk.ac.ed.pguaglia.real.runtime;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.function.Function;

public class Property<T extends Enum<T>> {

	public EnumSet<T> options;
	public T currentOption;

	private final Class<T> enumType;
	private String name;
	private T defaultOption;

	public Property(String name, Class<T> enumType, T defaultOption) {
		this.name = name;
		this.options = EnumSet.allOf(enumType);
		this.enumType = enumType;
		this.defaultOption = defaultOption;
		this.currentOption = defaultOption;
	}

	public T getDefaultOption() {
		return defaultOption;
	}

	public void setDefaultOption(T defaultOption) {
		this.defaultOption = defaultOption;
	}

	public String usage(String prefix) {
		return String.format("%s.%s [ %s ]", prefix, name, String.join(" | ", options()));
	}

	public String status(String prefix) {
		return String.format("%s\"%s\"", prefix, currentOption.toString());
	}

	public boolean parseOption(String str) {
		try {
			currentOption = Enum.valueOf(enumType, str);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	public String[] options() {
		return options(x -> x);
	}

	public String[] options(Function<String, String> fun) {
		return Arrays.stream(enumType.getEnumConstants()).map(Enum::name).map(fun).toArray(String[]::new);
	}
}
