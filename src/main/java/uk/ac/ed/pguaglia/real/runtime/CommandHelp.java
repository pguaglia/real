package uk.ac.ed.pguaglia.real.runtime;

import java.util.HashMap;

public class CommandHelp {

	public class Help {
		public final String usage;
		public final String desc;

		public Help(String usage, String desc) {
			this.usage = usage;
			this.desc = desc;
		}
	}

	private HashMap<String, Help> map = new HashMap<>();
	static CommandHelp instance = null;

	private CommandHelp() {
		map.put("syntax", new Help(syntax_usage, syntax_desc));
		map.put("tables", new Help(tables_usage, views_desc));
		map.put("views", new Help(views_usage, tables_desc));
		map.put("schema", new Help(schema_usage, schema_desc));
		map.put("help", new Help(help_usage, help_desc));
		map.put("quit", new Help(quit_usage, quit_desc));
		map.put("tree", new Help(tree_usage, tree_desc));
		map.put("eval", new Help(eval_usage, eval_desc));
		map.put("add", new Help(add_usage, add_desc));
		map.put("drop", new Help(drop_usage, drop_desc));
		map.put("load", new Help(load_usage, load_desc));
		map.put("save", new Help(save_usage, save_desc));
	}

	public static CommandHelp getInstance() {
		if (instance == null) {
			instance = new CommandHelp();
		}
		return instance;
	}

	public String getUsage(final String cmd) {
		return map.get(cmd).usage;
	}

	public String getDesc(final String cmd) {
		return map.get(cmd).desc;
	}

	public boolean hasCommand(final String cmd) {
		return map.containsKey(cmd);
	}

	private static final String syntax_usage = ".syntax";
	private static final String syntax_desc = "Show the Relational Algebra grammar accepted by REAL.";

	private static final String tables_usage = ".tables";
	private static final String tables_desc = "List names and attributes of the base tables in the current database.";

	private static final String views_usage = ".views";
	private static final String views_desc = "List names and definitions of the views in the current database.";

	private static final String schema_usage = ".schema";
	private static final String schema_desc = "Print the schema of the current database in JSON format.";

	private static final String help_usage = ".help [COMMAND]";
	private static final String help_desc = "Show help for COMMAND. "
			+ "Without an explicit COMMAND, the help message for all commands is shown.";

	private static final String quit_usage = ".quit";
	private static final String quit_desc = "Exit REAL's command prompt and go back to the shell "
			+ "(unsaved changes are discarded).";

	private static final String tree_usage = ".tree [on|off]";
	private static final String tree_desc = "Turn printing of the parse tree on/off [default: off]. " //
			+ "With no argument, the current setting is shown.";

	private static final String eval_usage = ".eval [set|bag|off]";
	private static final String eval_desc = "Choose query evaluation semantics:\n" //
			+ "- 'set': set semantics (default)\n" //
			+ "- 'bag': bag semantics\n" //
			+ "- 'off': no evaluation\n" //
			+ "With no arguments, the current setting is shown.";

	private static final String add_usage = ".add TABLE_NAME(ATTR_LIST) : DATA_FILE";
	private static final String add_desc = "Create a new base table:\n" //
			+ "- TABLE_NAME is the name of the table;\n" //
			+ "- ATTR_LIST is a comma-separated list of attribute names;\n" //
			+ "- DATA_FILE is the path to a CSV file containing the instance (rows) of the table.";

	private static final String drop_usage = ".drop TABLE_NAME";
	private static final String drop_desc = "Delete table or view TABLE_NAME from the schema.";

	private static final String load_usage = ".load PATH";
	private static final String load_desc = "Load the database from the JSON file specified by PATH.";

	private static final String save_usage = ".save [PATH]";
	private static final String save_desc = "Save the current database schema to the JSON file specified by PATH. "
			+ "Without PATH, the current schema file is used, which is the latest file explicitly loaded with .load, "
			+ "or the temporary file automatically created when starting the application.";
}
