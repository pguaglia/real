package uk.ac.ed.pguaglia.real.lang;

public abstract class UnaryOperation extends Expression {

	protected Expression operand;

	public UnaryOperation ( Expression operand, Expression.Type type ) {
		super(type);
		this.operand = operand;
	}

	public Expression getOperand() {
		return this.operand;
	}
}
