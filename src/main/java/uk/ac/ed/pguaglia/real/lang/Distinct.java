package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public class Distinct extends UnaryOperation {

	public Distinct(Expression operand) {
		super(operand, Type.DISTINCT);
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s\n%2$s |\n%2$s +--- %3$s";
		String conn = this.getType().getConnective();
		String s;
		if (operand.getType() == Expression.Type.BASE) {
			s = operand.toSyntaxTreeString("", schema);
			s += "\n" + prefix;
		} else {
			s = operand.toSyntaxTreeString(prefix + "      ", schema);
		}
		return String.format(tree, conn, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		return operand.signature(schema);
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		return bags ? operand.execute(db,true).distinct() : operand.execute(db,false);
	}

	@Override
	public String toString() {
		return String.format( "%s( %s )",
				this.getType().getConnective(),
				operand.toString() );
	}

	@Override
	public Set<String> getBaseNames() {
		return operand.getBaseNames();
	}
}
