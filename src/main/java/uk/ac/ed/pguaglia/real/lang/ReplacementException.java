package uk.ac.ed.pguaglia.real.lang;


@SuppressWarnings("serial")
public class ReplacementException extends Exception {

	private static final String nonInjectiveMapMsg =
			"The replacement map is not injective";
	private static final String renamedMoreThanOnceMsg =
			"Attribute \"%s\" renamed more than once";

	public ReplacementException(String msg) {
		super(msg);
	}

	public ReplacementException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public static ReplacementException nonInjectiveMap() {
		return new ReplacementException(nonInjectiveMapMsg);
	}

	public static ReplacementException renamedMoreThanOnce(String attr) {
		String msg = String.format(renamedMoreThanOnceMsg, attr);
		return new ReplacementException(msg);
	}
}
