package uk.ac.ed.pguaglia.real;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.text.WordUtils;

import com.opencsv.CSVWriterBuilder;

import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.Table;
import uk.ac.ed.pguaglia.real.lang.BaseExpression;
import uk.ac.ed.pguaglia.real.lang.Expression;

public class Utils {

	public static <T> Set<T> clone(Set<T> input) {
		return new HashSet<T>(input);
	}

	public static String[] getNames(Class<? extends Enum<?>> e) {
		return getNames(e, x -> x);
	}

	public static String[] getNames(Class<? extends Enum<?>> e, Function<String, String> f) {
		return Arrays.stream(e.getEnumConstants()).map(Enum::name).map(f).toArray(String[]::new);
	}

	public static String[] getNames(Class<? extends Enum<?>> e, String prefix) {
		return getNames(e, x -> "." + x);
	}

	public static <K, V> boolean isInjective(Map<K, V> input) {
		Map<V, K> inv = new HashMap<V, K>();
		for (Map.Entry<K, V> entry : input.entrySet()) {
			if (inv.containsKey(entry.getValue())) {
				return false;
			} else {
				inv.put(entry.getValue(), entry.getKey());
			}
		}
		return true;
	}

	public static String toSyntaxTreeString(Expression expr, Schema schema, String prefix, String add) {
		String s;
		if ((expr.getType() == Expression.Type.BASE) && ((schema == null) || ((BaseExpression) expr).isTable(schema))) {
			s = expr.toSyntaxTreeString("", schema);
			s += "\n" + prefix;
		} else {
			s = expr.toSyntaxTreeString(prefix + add, schema);
		}
		return s;
	}

	public static void tableToCSV(Table tbl, char sep, char quotes, boolean head) throws IOException {
		try (var writer = new CSVWriterBuilder(new OutputStreamWriter(System.out)).withSeparator(sep)
				.withQuoteChar(quotes).build()) {
			if (head) {
				var attr = tbl.getAttributes();
				var header = new String[attr.size()];
				attr.toArray(header);
				writer.writeNext(header, true);
			}
			writer.writeAll(tbl.getRecords(), true);
		}
	}

	public static String wrapListItem(String line, String label, int wrapWidth) {
		if (line.startsWith(label) == false) {
			return WordUtils.wrap(line, wrapWidth);
		}
		int indentLen = label.length();
		String[] wrappedLines = Arrays
				.stream(WordUtils.wrap(line.substring(indentLen), wrapWidth - indentLen).split("\n"))
				.toArray(String[]::new);
		return label + String.join("\n" + " ".repeat(indentLen), wrappedLines);
	}

	public static String centerLine(String line, int width) {
		int lineLen = line.length();
		if (width <= lineLen) {
			return line;
		}
		int paddingLeft = (width - lineLen) / 2;
		int paddingRight = width - (lineLen + paddingLeft);
		return " ".repeat(paddingLeft).concat(line).concat(" ".repeat(paddingRight));
	}
}
