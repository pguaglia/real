package uk.ac.ed.pguaglia.real.lang;

import java.util.Map;
import java.util.Set;

import uk.ac.ed.pguaglia.real.parsing.RALexer;

public abstract class Condition {

	private static String getConnective(int tokenID) {
		String literal = RALexer.VOCABULARY.getLiteralName(tokenID);
		return literal.replaceAll("'", "");
	}

	public static enum Type {
		COMPARISON  (""),
		CONJUNCTION (Condition.getConnective(RALexer.AND)),
		DISJUNCTION (Condition.getConnective(RALexer.OR)),
		NEGATION    (Condition.getConnective(RALexer.NOT));

		private final String connective;

		private Type ( String connective ) {
			this.connective = connective;
		}

		public String getConnective() {
			return connective;
		}
	}

	private final Type type;

	public Condition (Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public String parenthesize() {
		String s = this.toString();
		switch ( this.getType() ) {
		case DISJUNCTION:
		case CONJUNCTION:
			return String.format("( %s )", s);
		default:
			return s;
		}
	}

	public abstract boolean satisfied(String[] record, Map<String,Integer> attr);

	public abstract Set<String> signature();
}
