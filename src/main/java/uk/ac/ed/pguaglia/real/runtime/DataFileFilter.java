package uk.ac.ed.pguaglia.real.runtime;

import java.io.File;
import java.io.FileFilter;

public class DataFileFilter implements FileFilter {

	public final static String CSV_FILE_EXTENSION = ".csv";

	@Override
	public boolean accept(File pathname) {
		return pathname.exists() && pathname.isFile() && pathname.toString().toLowerCase().endsWith(CSV_FILE_EXTENSION);
	}
}
