package uk.ac.ed.pguaglia.real.runtime;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jline.builtins.Completers.TreeCompleter;
import org.jline.reader.EOFError;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.ParsedLine;
import org.jline.reader.impl.DefaultParser;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;

import uk.ac.ed.pguaglia.real.Utils;
import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.SchemaMapper;
import uk.ac.ed.pguaglia.real.db.Table;
import uk.ac.ed.pguaglia.real.lang.Expression;
import uk.ac.ed.pguaglia.real.lang.ReplacementException;

public final class CommandLineApp {

	private static final String PROMPT = "$0 >>> ";

	private static final char UPPER_LEFT_CORNER = '┌';
	private static final char LOWER_LEFT_CORNER = '└';
	private static final char UPPER_RIGHT_CORNER = '┐';
	private static final char LOWER_RIGHT_CORNER = '┘';
	private static final char VBAR = '│';
	private static final char HBAR = '─';

	private static final String TOP_BAR = UPPER_LEFT_CORNER + String.valueOf(HBAR).repeat(78) + UPPER_RIGHT_CORNER;
	private static final String BOTTOM_BAR = LOWER_LEFT_CORNER + String.valueOf(HBAR).repeat(78) + LOWER_RIGHT_CORNER;
	
	private static final String TITLE_LINE = "REAL: an interpreter for Relational Algebra (v%s)";
	private static final String LICENSE_LINE = "Released under the MIT License";
	private static final String COPYRIGHT_LINE = "Copyright © 2019-2023 Paolo Guagliardo";
	

	private enum Tree { ON, OFF }
	private enum Eval { OFF, SET, BAG }

	private enum Commands {
		HELP,   // show help for commands
		QUIT,   // exit the application
		TREE,   // show the parse tree [on/off]
		EVAL,   // set evaluation semantics [set/bag/off]
		DROP,   // delete table or view
		SAVE,   // save current database
		LOAD,   // load database from file
		ADD,    // create new base table
		TABLES, // list tables
		VIEWS,  // list views
		SCHEMA, // print the JSON schema file (private)
		SYNTAX, // show accepted RA syntax
	}

	private static class MyLineParser extends DefaultParser {

		public ParsedLine parse(final String line, final int cursor, ParseContext context) {

			boolean dotStart = line.stripLeading().startsWith(".");
			boolean semicolonEnd = line.stripTrailing().endsWith(";");

			if (context != ParseContext.COMPLETE && context != ParseContext.SPLIT_LINE) {
				if (dotStart == false && semicolonEnd == false) {
					throw new EOFError(-1, -1, "No end semicolon", "semicolon");
				}
			}
			return super.parse(line, cursor, context);
		}
	}

	private static final String APP_COMMAND = "java -jar <path/to/real-X.Y.Z.jar>";

	private static final String OPT_DATA_SHORT = "d";
	private static final String OPT_DATA_LONG  = "database";

	private static final String OPT_EXPR_SHORT = "q";
	private static final String OPT_EXPR_LONG  = "query";

	private static final String OPT_EVAL_SHORT = "e";
	private static final String OPT_EVAL_LONG  = "eval";

	private static final String OPT_TREE_SHORT = "t";
	private static final String OPT_TREE_LONG  = "tree";

	private static final String OPT_CSV_SHORT  = "csv";

	private static final Options setupCliOptions() {
		final Options options = new Options();
		options.addOption(Option.builder(OPT_DATA_SHORT).longOpt(OPT_DATA_LONG)
				.hasArg().argName("path/to/database/file").build());
		options.addOption(Option.builder(OPT_EXPR_SHORT).longOpt(OPT_EXPR_LONG)
				.hasArg().argName("expression").build());
		options.addOption(Option.builder(OPT_EVAL_SHORT).longOpt(OPT_EVAL_LONG)
				.hasArg().argName("semantics").build());
		options.addOption(Option.builder(OPT_TREE_SHORT).longOpt(OPT_TREE_LONG)
				.hasArg(false).build());
		options.addOption(Option.builder(OPT_CSV_SHORT)
				.hasArg().argName("options").build());
		return options;
	}

	private static Properties CSVProperties = new Properties();

	public static final void main( String[] args ) {

		final String version = CommandLineApp.class.getPackage().getImplementationVersion();

		Options opts = setupCliOptions();
		HelpFormatter formatter = new HelpFormatter();

		CommandLineParser optParser = new org.apache.commons.cli.DefaultParser();
		CommandLine cliCmd = null;
		try {
			cliCmd = optParser.parse( opts, args );
		} catch (MissingOptionException e2) {
			formatter.printHelp( APP_COMMAND, opts );
			System.exit(-1);
		} catch (ParseException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		// load help and syntax messages from file
		String helpText = null;
		String syntax = null;
		try {
			InputStream input = CommandLineApp.class.getResourceAsStream("/help.txt");
			helpText = new String(input.readAllBytes());
			input = CommandLineApp.class.getResourceAsStream("/syntax.txt");
			syntax = new String(input.readAllBytes());
			input.close();
		} catch (IOException e3) {
			e3.printStackTrace();
		}

		Property<Tree> parseTree = new Property<>(
				Commands.TREE.toString().toLowerCase(),
				Tree.class,
				Tree.OFF );

		if (cliCmd.hasOption(OPT_TREE_SHORT)) {
			parseTree.currentOption = Tree.ON;
		}

		Property<Eval> evalQuery = new Property<>(
				Commands.EVAL.toString().toLowerCase(),
				Eval.class,
				Eval.SET );

		@SuppressWarnings("unused")
		Property<Commands> commands = new Property<>(
				"help",
				Commands.class,
				null );

		if (cliCmd.hasOption(OPT_EVAL_SHORT)) {
			String arg = cliCmd.getOptionValue(OPT_EVAL_SHORT);
			if (evalQuery.parseOption(arg.toUpperCase()) == false) {
				System.err.println(String.format("Unrecognized value \"%s\"%nPossible values are [ %s ]", arg,
						String.join(" | ", evalQuery.options())));
				System.exit(-1);
			}
		}

		if (cliCmd.hasOption(OPT_CSV_SHORT)) {
			String[] csvArgs = cliCmd.getOptionValue(OPT_CSV_SHORT).split(" ");
			if (csvArgs.length != 3) {
				System.err.println(String.format("Unrecognized CSV options \"%s\"%nFormat: \"header delimiter quotes\"",
						cliCmd.getOptionValue(OPT_CSV_SHORT)));
				System.exit(-1);
			}
			if (csvArgs[0].matches("[yYtT1]")) {
				CSVProperties.setProperty("HEADER", "1");
			} else if (csvArgs[0].matches("[nNfF0]")) {
				CSVProperties.setProperty("HEADER", "0");
			} else {
				System.err.println(
						String.format("Unrecognized CSV header option \"%s\"%nFormat: \"[ynYNtfTF]\"", csvArgs[0]));
				System.exit(-1);
			}
			if (csvArgs[1].matches(".")) {
				CSVProperties.setProperty("SEPARATOR", csvArgs[1]);
			} else {
				System.err.println("CSV separator must be a single character");
				System.exit(-1);
			}
			if (csvArgs[2].matches(".")) {
				CSVProperties.setProperty("QUOTES", csvArgs[2]);
			} else {
				System.err.println("CSV quotes must be a single character");
				System.exit(-1);
			}
		}

		final boolean batch = cliCmd.hasOption(OPT_EXPR_SHORT);

		var subtrees = new ArrayList<TreeCompleter.Node>();
		Function<String,String> lower = x -> x.toLowerCase();
		for (var command : Commands.values()) {
			String name = "." + command.toString().toLowerCase();
			switch (command) {
			case EVAL:
				subtrees.add(TreeCompleter.node(name, enumToNode(Eval.class, lower)));
				break;
			case TREE:
				subtrees.add(TreeCompleter.node(name, enumToNode(Tree.class, lower)));
				break;
			case HELP:
				subtrees.add(TreeCompleter.node(name, enumToNode(Commands.class, lower)));
				break;
			default:
				subtrees.add(TreeCompleter.node(name));
			}
		}
		TreeCompleter completer = new TreeCompleter(subtrees);

		Terminal terminal = null;
		Database db = null;
		try {
			terminal = batch ? null : TerminalBuilder.terminal();
			if (cliCmd.hasOption(OPT_DATA_SHORT)) {
				File src = new File(cliCmd.getOptionValue(OPT_DATA_SHORT));
				db = new Database(src);
			} else {
				db = new Database();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		LineReader lineReader = null;
		if (batch == false) {
			MyLineParser lineParser = new MyLineParser();
			lineParser.setEscapeChars(new char[0]);
			lineReader = LineReaderBuilder.builder()
					.terminal(terminal)
					.parser(lineParser)
					.variable(LineReader.SECONDARY_PROMPT_PATTERN, " %N %P. ")
					.completer(completer)
					.build();

			System.out.println(TOP_BAR);
			System.out.printf("%s %s %s%n", VBAR, Utils.centerLine(String.format(TITLE_LINE, version), 76), VBAR);
			System.out.printf("%s %s %s%n", VBAR, Utils.centerLine(LICENSE_LINE, 76), VBAR);
			System.out.printf("%s %s %s%n", VBAR, Utils.centerLine(COPYRIGHT_LINE, 76), VBAR);
			System.out.println(BOTTOM_BAR);
		}
		String viewName = null;

		mainLoop: do {
			String line = null;
			if (batch == false) {
				line = lineReader.readLine(PROMPT).strip();
				if (line.startsWith(".")) {
					int spc = line.indexOf(" ");
					String cmdName;
					if (spc > 0) {
						cmdName = line.substring(1,spc);
						line = line.substring(spc).strip();
					} else {
						cmdName = line.substring(1);
						line = "";
					}
					Commands cmd;
					try {
						cmd = Commands.valueOf(cmdName.toUpperCase());
					} catch (IllegalArgumentException e) {
						System.err.println(String.format("Unrecognized command \".%s\"\n"
								+ "Type \".help\" to print the list of available commands.", cmdName));
						continue mainLoop;
					}
					cmdSwitch: switch (cmd) {
					case QUIT:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						break mainLoop;
					case HELP:
						var help = CommandHelp.getInstance();
						if (line.isBlank()) {
							//String out = String.join("\n", commands.options(x -> formattedHelp(x.toLowerCase())));
							//System.out.printf("%n%s%n", out);
							System.out.println(helpText);
						} else if (help.hasCommand(line)) {
							System.out.printf("%n%s%n", formattedHelp(line));
						} else {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						continue mainLoop;
					case SYNTAX:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						System.out.println(syntax);
						continue mainLoop;
					case TABLES:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						print(Commands.TABLES, db.schema());
						continue mainLoop;
					case VIEWS:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						print(Commands.VIEWS, db.schema());
						continue mainLoop;
					case SCHEMA:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						try {
							String info = SchemaMapper.getInstance().writerWithDefaultPrettyPrinter()
									.writeValueAsString(db.schema());
							System.out.println(info);
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
						break cmdSwitch;
					case TREE:
						if (line.isBlank()) {
							System.out.println(parseTree.status("Printing of syntax tree is "));
						} else if (parseTree.parseOption(line.toUpperCase()) == false) {
							System.err.println(String.format("Unrecognized option \"%s\"%n%s", line, parseTree.usage("USAGE: ")));
						}
						break cmdSwitch;
					case EVAL:
						if (line.isBlank()) {
							System.out.println(evalQuery.status("Query evaluation is "));
						} else if (evalQuery.parseOption(line.toUpperCase()) == false) {
							System.err.println(String.format("Unrecognized option \"%s\"%n%s", line, evalQuery.usage("USAGE: ")));
						}
						break cmdSwitch;
					case DROP:
						String name = line.strip();
						try {
							db.schema().drop(name);
							System.out.println(String.format("INFO: deleted %s", name));
						} catch (Exception e) {
							System.err.println(String.format("ERROR: %s", e.getMessage()));
							System.err.flush();
						}
						continue mainLoop;
					case LOAD:
						try {
							File src = new File(line.strip());
							db = new Database(src);
						} catch (IOException e) {
							System.out.println(String.format("ERROR: %s", e.getMessage()));
						}
						continue mainLoop;
					case SAVE:
						save(db, line, false);
						continue mainLoop;
					case ADD:
						addTable(line, db.schema());
						continue mainLoop;
					}
					continue mainLoop;
				}
			} else {
				line = cliCmd.getOptionValue(OPT_EXPR_SHORT);
			}
			line = line.stripTrailing();
			line = line.endsWith(";") ? line.substring(0, line.length() - 1) : line;
			if (line.isBlank() == true) {
				continue mainLoop;
			}
			if (line.contains(":=")) {
				String[] viewDef = line.split(":=");
				viewName = viewDef[0].strip();
				line = viewDef[1].strip();
			}

			Expression expr = null;
			try {
				expr = Expression.parse(line);
			} catch (RecognitionException e1) {
				System.err.println(String.format("ERROR: Cannot parse expression \"%s\".", line));
				System.err.flush();
				continue mainLoop;
			} catch (ReplacementException e2) {
				System.err.println(String.format("ERROR: In \"%s\": %s", e2.getMessage(), e2.getCause().getMessage()));
				System.err.flush();
				continue mainLoop;
			}

			if (parseTree.currentOption == Tree.ON) {
				System.out.println(expr.toSyntaxTreeString());
			}
			if (viewName != null) {
				try {
					expr.signature(db.schema());
					db.schema().addView(viewName,expr);
				} catch (SchemaException e) {
					System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
					System.err.flush();
				} finally {
					viewName = null;
				}
				continue mainLoop;
			}

			if (evalQuery.currentOption == Eval.OFF) {
				try {
					expr.signature(db.schema());
				} catch (SchemaException e) {
					System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
					System.err.flush();
				}
				continue mainLoop;
			}

			try {
				Table answer = db.execute(expr, evalQuery.currentOption == Eval.BAG);
				if (batch) {
					char sep = ((String) CSVProperties.getOrDefault("SEPARATOR", ",")).charAt(0);
					char quotes = ((String) CSVProperties.getOrDefault("QUOTES", "\"")).charAt(0);
					boolean head = ((String) CSVProperties.getOrDefault("HEADER", "1")).equals("1") ? true : false;
					Utils.tableToCSV(answer, sep, quotes, head);
				} else {
					System.out.println(answer);					
				}
			} catch (SchemaException e) {
				System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
				System.err.flush();
				if (batch == true) {
					System.exit(1);
				}
				continue mainLoop;
			} catch (IllegalStateException e) {
				System.err.println(String.format("ERROR: In \"%s\": %s", expr, e.getMessage()));
				System.err.flush();
				if (batch == true) {
					System.exit(-1);
				}
				continue mainLoop;
			} catch (Exception e) {
				e.printStackTrace();
				System.err.flush();
				if (batch == true) {
					System.exit(-1);
				}
				continue mainLoop;
			}
		} while (batch == false);
	}

	private static boolean save(Database db, String line, boolean overwrite) {
		try {
			if (line.isBlank()) {
				return db.save();
			}
			File f = new File(line.strip());
			return db.save(f, overwrite);
		} catch (IOException e) {
			String msg = String.format("ERROR: %s. File not saved", e.getMessage());
			System.out.println(msg);
		}
		return false;
	}

	private static void addTable(String line, Schema sch) {
		Pattern p = Pattern.compile("\\w+\\(\\w+(,\\w+)*\\):.+");
		Matcher m = p.matcher(line.replaceAll("\\s", ""));
		if (m.matches() == false) {
			System.err.println(String.format("ERROR: Unrecognized table specification \"%s\"", line));
			System.out.printf("USAGE:%n%s", formattedHelp("add"));
			return;
		}
		String[] lines = line.split(":");
		String filename = lines[1].strip();
		String spec = lines[0];
		int start = spec.indexOf("(");
		int end = spec.indexOf(")");
		String tableName = spec.substring(0,start).strip();
		List<String> attributes = new ArrayList<>();
		for (String attr : spec.substring(start+1,end).split(",")) {
			attributes.add(attr.strip());
		}
		try {
			sch.addTable(tableName, attributes, new File(filename));
		} catch (Exception e) {
			System.err.println("ERROR: " + e.getMessage());
		}
	}

	private static void print(Commands cmd, Schema sch) {
		Set<String> names;
		String noNamesMsg = String.format("NO %s FOUND", cmd.toString().toUpperCase());
		String fmtSuffix = "";
		switch (cmd) {
		case TABLES:
			names = sch.getTableNames();
			fmtSuffix = " : { %s }";
			break;
		case VIEWS:
			names = sch.getViewNames();
			fmtSuffix = " := %s";
			break;
		default:
			throw new IllegalStateException();
		}
		if (names.isEmpty()) {
			System.out.println(noNamesMsg);
			return;
		}
		int m = names.stream().map(String::length).max((x,y) -> x-y).get();
		String fmt = "%" + m + "s" + fmtSuffix;
		for (String name : names) {
			if (cmd == Commands.TABLES) {
				System.out.println(String.format(fmt, name, String.join(", ", sch.getTableAttributes(name))));
			}
			if (cmd == Commands.VIEWS) {
				System.out.println(String.format(fmt, name, sch.getViewDefinition(name).toString()));
			}
		}
	}

	private static TreeCompleter.Node enumToNode(Class<? extends Enum<?>> e, Function<String,String> f) {
		return TreeCompleter.node(Arrays.stream(e.getEnumConstants()).map(Enum::name).map(f).toArray());
	}

	private static String formattedHelp(String cmd) {
		var help = CommandHelp.getInstance();
		String usage = help.getUsage(cmd);
		String desc = String.join("\n", Arrays.stream(help.getDesc(cmd).split("\n"))
				.map(x -> Utils.wrapListItem(x, "- ", 78)).toArray(String[]::new));
		return String.format("%s%n%s", "  " + usage + System.lineSeparator(), "  " + desc + System.lineSeparator());

	}
}
