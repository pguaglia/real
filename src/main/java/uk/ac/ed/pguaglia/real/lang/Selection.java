package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import uk.ac.ed.pguaglia.real.Utils;
import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public class Selection extends UnaryOperation {

	private Condition cond;

	public Selection ( Condition cond, Expression operand ) {
		super(operand,Expression.Type.SELECTION);
		this.cond = cond;
	}

	public Condition getCondition() {
		return cond;
	}

	@Override
	public String toString() {
		return String.format( "%s[%s]( %s )",
				this.getType().getConnective(),
				cond.toString(),
				operand.toString() );
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s[%2$s]\n%3$s │\n%3$s └─── %4$s";
		String conn = this.getType().getConnective();
		String cond = this.cond.toString()//.replace(" ", "");
				.replace("( ", "(")
				.replace(" )", ")")
				.replace(" = ", "=");
		String s = Utils.toSyntaxTreeString(operand, schema, prefix, "      ");
		return String.format(tree, conn, cond, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> condAttrs = cond.signature();
		Set<String> exprAttrs = Utils.clone(operand.signature(schema));
		if (exprAttrs.containsAll(condAttrs)) {
			return exprAttrs;
		} else {
			condAttrs.removeAll(exprAttrs);
			String attr = condAttrs.iterator().next();
			throw SchemaException.attributeNotFound(attr, this, schema);
		}
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		return operand.execute(db,bags).select(cond);
	}

	@Override
	public Set<String> getBaseNames() {
		return operand.getBaseNames();
	}
}
