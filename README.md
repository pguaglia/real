# REAL

`real` is a command-line Java application to parse and execute Relational Algebra queries.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

- Java SE Development Kit 12 or later
- [Apache Maven](https://maven.apache.org/) 3.6+
- Optional: Git (any reasonably recent version)

### Building

#### From stable sources

1. Download a stable release from <https://git.ecdf.ed.ac.uk/pguaglia/real/releases>.

2. Unpack the archive anywhere on your system.

3. `cd` to the folder `real-<version>`, where `<version>` is the release version number.

4. Build the project with Maven:
```
mvn package
```
This will create the file `real-<version>.jar` in the `target/` subfolder.

#### From development sources

1. Clone the repository with
```
git clone https://git.ecdf.ed.ac.uk/pguaglia/real.git
```

2. `cd` to the repository folder (`real/`)

3. Switch to the development branch with
```
git checkout --track origin/develop
```

4. Build the project with Maven:
```
mvn package
```
This will create the file `real-<version>-SNAPSHOT.jar` in the `target/` subfolder.

### Installing

You can move the file `real-<version>.jar` to any location on your system. The
JAR contains all the necessary dependencies to be executed.

### Running

You can run the `real` executable JAR with
```
java -jar <install-path>/real-<version>.jar <path/to/csv/folder>
```
where
- `<install-path>` is the path to the folder where the `real` JAR file is
located, and
- `<path/to/csv/folder>` is the path to the folder containing the CSV files that
will populate the database.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
