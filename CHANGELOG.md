# REAL changelog

## Version [0.7](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.7) (2023-09-25)

#### Changed functionality

- The behavior of the *renaming* operation `<R>` was reverted to use functional
  overrides of the identity mapping over attribute names. More precisely, each
  replacement `OLD->NEW` overrides the default mapping `OLD->OLD`, as long as
  the resulting mapping &ndash; obtained by considering all of the specified
  replacements &ndash; is functional (i.e., no pair of distinct attributes are
  mapped to the same name). Moreover, the comma-separated list of replacements
  of a renaming operation must not contain two entries with the same left-hand
  side (i.e., an attribute cannot be renamed twice in the same operation).

#### Enhancements

- **Downgraded to Java 11**. This is for compatibility with the Java version
  installed on the student DICE machines (including the `student.login` and
  `student.compute` servers). The functionality of the REAL interpreter is
  unchanged.

## Version [0.6.2](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.6.2) (2022-11-15)

#### Bug fixes

- Fixed warning about the creation of a dumb terminal when REAL is executed in
  batch mode inside a shell script (issue #16).

#### Enhancements

- Return non-zero exit code on error when in batch mode (issue #12). Exit code 1
  indicates a syntax or validation error in the query expression provided with
  the `-q` command-line option when executed on the database specified with the
  `-d` command-line option.

- Include the *duplicate elimination* operation `<E>` in the Relational Algebra
  grammar supported by REAL, as printed with the `.syntax` command (issue #15).

## Version [0.6.1](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.6.1) (2022-09-27)

#### Bug fixes

- Included the Jansi library in the JAR file to avoid the creation of a "dumb"
  terminal in Windows.

- Fixed the regular expression for the syntax used by the `.add` command, which
  prevented the addition of tables with attribute names, other than the first,
  consisting of more than one character.

## Version [0.6](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.6) (2022-04-05)

#### New features

- **Multi-line input**. Relational Algebra expressions issued interactively at
  the command prompt can now be broken into multiple lines by pressing the
  RETURN key. If new lines are added after the first one, the initial prompt
  changes from `$0 >>> ` to ` N ... ` where `N` is the number (&geq;1) of the
  current line. The input is considered complete when terminated by semicolon
  `;`, at which point the whole expression is fed to the parser. Multi-line
  input is not available for internal commands, which therefore need not be
  terminated by semicolon.

- The format of the **CSV output in batch mode** can be controlled with a newly
  introduced `-csv` option. This takes an argument of the form

  `<header> <separator> <quotes>`

  where:
  - `<header>` indicates whether the first line of the CSV output is the list of
    attributes of the answer table; the values `1`, `t`, `T`, `y`, `Y` denote
    "true" (default) and `0`, `f`, `F`, `n`, `N` denote "false"
  - `<separator>` is a single character to use as a separator for fields; the
    default is comma `,`
  - `<quotes>` is a single character used for quoting each field; the default is
     double quotation marks `"`

  For example, `-csv "0 | \""` sets the header off and uses the pipe `|` as a
  delimiter instead of the default comma `,`.

- Interactive suggestions and **auto-completion of internal commands**. Pressing
  the TAB key while typing a command name (starting with dot `.`) displays a
  list of possible completions for the command. Repeatedly pressing TAB cycles
  through the available options, which can also be navigated using the arrow
  keys. The highlighted option can be selected by pressing RETURN. If only one
  completion is possible, this is chosen automatically. Completion of options is
  available for commands that take arguments (e.g., `.help`).

#### Changed functionality

- Relational Algebra expressions issued at the interactive prompt must now be
  terminated by semicolon `;` in order to be parsed and executed; otherwise,
  multi-line input is assumed. This is not required in batch mode (as before)
  for expressions supplied with the `-q` option.

#### Enhancements

- The welcome message (with version, license and copyright information) is now
  omitted when the app is executed in batch mode (issue #6).
- Separate in-app help for each internal command (issue #14). The `.help`
  command now takes the name of a command (without the starting dot `.`) as an
  optional argument and displays the usage information and description of that
  command only. For example, `.help tables` will display the help message for
  the `.tables` command. Without argument, `.help` shows the usage information
  and descripton of all available commands.
- Improved formatting of syntax trees, using smoother and more readable lines.

#### Bug fixes

- Prevent application from crashing when a non-conforming input is passed to
  `.add` in the command prompt (issue #13). An error message is now displayed,
  along with the usage information for the command.

## Version [0.5.3](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.5.3) (2021-10-21)

#### Bug fixes

- Fixed a bug in the implementation of intersection (issue #11).

## Version [0.5.2](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.5.2) (2021-10-08)

#### Bug fixes

- Fixed a bug in the implementation of difference (issue #10).

## Version [0.5.1](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.5.1) (2021-10-04)

#### Bug fixes

- View dependencies are now updated on deletion (issue #5).
- Fixed a bug in the sort-merge implementation of the difference operation.

## Version [0.5](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.5) (2021-09-28)

#### New features

- In-app help on internal commands. Issuing the command `.help` at REAL's prompt
  shows the list of available commands, along with their usage and description.
- Atomic selection conditions now support less-than comparisons using the newly
  introduced `<` operator. This compares strings lexicographically, but behaves
  differently on strings that represent integer values. More precisely, if the
  two strings being compared can be *both* parsed as integers, then they are
  compared as integers; otherwise, lexicographic comparison is used. As a side
  effect, we lose the ability to compare lexicographically strings that consist
  only of digits. This will be rectified once proper types are introduced.

#### Changed functionality

- Renaming is now defined as an operation that only accepts one replacement, in
  place of a list thereof. That possibility is retained as a syntactic shortcut,
  which is parsed as a sequence of nested renamings. For example,
  `<R>[A->B,B->C](table)` is short for `<R>[B->C](<R>[A->B](table))`.

#### API improvements

- Get list of replacements in the renaming operation.
- Getters for conditions and terms.
- New abstract superclass for unary operations.

## Version [0.4](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.4) (2020-09-08)

#### New features

- Adding tables via the interactive prompt. This can be done by issuing the
  following command:

  ```.add TABLE-NAME( COMMA-SEPARATED-LIST-OF-ATTRIBUTES ) : PATH-TO-CSV-FILE```

- Database schema. By default, the application starts with an empty database
  schema that can be populated with
  * tables, using the newly introduced `.add` command;
  * views, using the existing syntax `VIEW-NAME := VIEW-DEFINITION`.

  The schema now records the correspondence between a table name and the CSV
  file containing its data, which is loaded into main memory only when needed to
  evaluate a query. Attribute names are not recorded in the CSV files, but in
  the schema.

- Persistent views. The schema can be serialized to JSON with the `.save
  [PATH-TO-JSON-FILE]` command for reuse across sessions. If invoked without
  specifying the path to a file, `.save` uses either
  * the file in which the previously loaded schema is stored (see below), or
  * the temporary file created when starting the application, if no schema was
    previously loaded.

  Note that the values of the configuration parameters (e.g., `.eval` and
  `.tree`) are not saved in the exported JSON file.

  A schema file in JSON format can be loaded using either:
  * the command-line switch `-d` (previously used to specify the CSV files to
    import), or
  * the command `.load PATH-TO-JSON-FILE` at the interactive prompt.

- The `.drop` command can now also delete tables as well as views.

#### Changed functionality

- The command-line switch `-d` now expects a JSON schema file rather than the
  path to a folder containing CSV files.
- The dependency on SQLite is no longer needed.

## Version [0.3](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.3) (2020-04-08)

#### New features

- Command line options:
  * `-d`, `--data` to specify the data directory with the CSV files,
  * `-e`, `--eval` to control the query execution semantics (corresponds to the
    `.eval` command in interactive mode),
  * `-q`, `--query` to specify a query expression to be executed in a batch way,
  * `-t`, `--tree` to control the printing of syntax trees (corresponds to the
    `.tree` command in interactive mode).

- Views (virtual tables). In interactive mode, a view is created with the syntax
  `VIEW-NAME := EXPRESSION`, where `EXPRESSION` may mention previously defined
  views. A view can be deleted with the command `.drop VIEW-NAME`, which results
  in an error if the view to be deleted is still used (directly or indirectly)
  in the definition of another view. Views do not persist across runs of the
  application.

#### Bug fixes

- A blank input at the command prompt now does not cause an EmptyStackException
  to be thrown (issue #2).

## Version [0.2.1](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.2.1) (2020-03-02)

#### Bug fixes

- The union operation now returns the correct answers under set semantics.

## Version [0.2](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.2) (2020-02-27)

#### New features

- Evaluation under bag semantics. The evaluation of RA expressions (`.eval`) can
  now be set to `OFF`, `SET` (set semantics; default) or `BAG` (bag semantics).

- Added duplicate elimination. Under bag semantics, this RA operation removes
  duplicates (multiple occurrences of the same row) from a table.

## Version [0.1](https://git.ecdf.ed.ac.uk/pguaglia/real/-/releases/0.1) (2020-01-30)

#### New features

- Import database tables from the CSV files in a given directory. For each CSV
  file, a table with the same name is created; its column names (table header)
  are taken from the first row of values in the file.

- Evaluate RA expressions under set semantics. Query execution is controlled by
  the `.eval` switch, which can be set to `ON` (default) or `OFF` (expressions
  are validated against the database schema, but they are not executed).

- Display the syntax tree of RA expressions. This is controlled by the `.tree`
  option, which can be set to `ON` (show the syntax tree) or `OFF` (default).
